import json, numpy
# 1D Heisenberg model with open-boundary condition.
# doi: 10.21468/SciPostPhys.6.3.029

nsite = 10

data = {}

# zero-based
h1_list = ["1*"+"I"*(2*i+1)+"XX"+"I"*(nsite-2*i-3) for i in range(nsite//2-1)] + \
        ["1*"+"I"*(2*i+1)+"YY"+"I"*(nsite-2*i-3) for i in range(nsite//2-1)] + \
        ["1*"+"I"*(2*i+1)+"ZZ"+"I"*(nsite-2*i-3) for i in range(nsite//2-1)]

h2_list = ["1*"+"I"*(2*i)+"XX"+"I"*(nsite-2*i-2) for i in range(nsite//2)] + \
        ["1*"+"I"*(2*i)+"YY"+"I"*(nsite-2*i-2) for i in range(nsite//2)] + \
        ["1*"+"I"*(2*i)+"ZZ"+"I"*(nsite-2*i-2) for i in range(nsite//2)]


data['hao'] = h1_list + h2_list

data['h1ao'] = h2_list

# reference state is the gs of h1
op_ansatz = [[
    "-1*"+"I"*(2*i+1)+"XX"+"I"*(nsite-2*i-3),
    "-1*"+"I"*(2*i+1)+"YY"+"I"*(nsite-2*i-3),
    "-1*"+"I"*(2*i+1)+"ZZ"+"I"*(nsite-2*i-3),
    ] for i in range(nsite//2-1)] + \
    [[
    "-1*"+"I"*(2*i)+"XX"+"I"*(nsite-2*i-2),
    "-1*"+"I"*(2*i)+"YY"+"I"*(nsite-2*i-2),
    "-1*"+"I"*(2*i)+"ZZ"+"I"*(nsite-2*i-2),
    ] for i in range(nsite//2)]

data["op_ansatz"] = op_ansatz

data['ref_state'] = "inp.py"

with open('incar', 'w') as f:
    json.dump(data, f, indent=4)
