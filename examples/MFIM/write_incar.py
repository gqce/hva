import json
# 1D mixed-field Ising model with open-boundary condition.

nsite = 16
jz = -1
hz = 0.5
hx = -1

labels = []

data = {}

hx_list = [f'{hx}*'+"I"*i+"X"+"I"*(nsite-i-1) for i in range(nsite)]
hz_list = [f'{hz}*'+"I"*i+"Z"+"I"*(nsite-i-1) for i in range(nsite)]
zz_list = [f'{jz}*'+"I"*i+"ZZ"+"I"*(nsite-i-2) for i in range(nsite-1)]
data['hao'] = hx_list + hz_list + zz_list

op_ansatz = [zz_list, hz_list, hx_list]
data["op_ansatz"] = op_ansatz

pool = ["I"*i+"Y"+"I"*(nsite-i-1) for i in range(nsite)]
pool.extend(['I'*i+op[0]+'I'*(j-i-1)+op[1]+'I'*(nsite-j-1)
        for op in ['YZ', 'ZY']
        for i in range(nsite-1)
        for j in range(i+1, nsite)
        ])
data['pool'] = pool
data['ref_state'] = "1"*nsite

with open('incar', 'w') as f:
    json.dump(data, f, indent=4)
