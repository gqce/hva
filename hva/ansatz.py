# author: Yongxin Yao (yxphysice@gmail.com)
import numpy
import scipy.optimize
from timing import timeit



class ansatz:
    def __init__(self,
            model,           # spin model
            ref_state=None,  # reference state of the ansatz
            refmode=0,       # reference state mode. 0: h1ao gs; 1: hf state
            opspm=True,      # operator in scipy sparse matrix
            params=None,     # initial parameter values
            nlayers=1,       # number of layers for the circuits
            maxiter=None,    # max iteration
            ):
        self._opspm = opspm
        self._nq = model._nsite
        self._model = model
        # set reference state
        self.set_ref_state(ref_state, refmode)
        self._nlayers = nlayers
        self._maxiter = maxiter
        if params is None:
            self._params = numpy.zeros(3*nlayers)
        else:
            self._params = params
        # list of generators
        self._generators = []
        if opspm:
            self._h = model._h.data.tocsr()
        else:
            self._h = model._h

        # current state vector
        self._state = None
        self.set_ansatz()

    def get_cost(self):
        '''
        get the value of the cost function (Hamiltonian expectation value).
        '''
        # have the state vector updated.
        self.update_state()
        # expectation value of the Hamiltonian
        if self._opspm:
            res = numpy.vdot(self._state, self._h.dot(self._state))
        else:
            res = self._h.matrix_element(self._state, self._state)
        return res.real

    @timeit
    def optimize_params(self):
        # full reoptimization of the ansatz given the initial point.
        res = scipy.optimize.minimize(fun_cost,
                self._params,               # starting parameter point
                args=(self._generators,
                        self._ref_state,    # reference state
                        self._h,            # Hamiltonian
                        ),
                method='BFGS',              # conjugate gradient
                jac=fun_jac,                # analytical jacobian function
                options={'maxiter': self._maxiter},
                )
        # print(res)
        # save parameters and cost.
        self._params = res.x.tolist()
        self._cost = res.fun
        self._result = res
        print(f"nfev = {res.nfev}, njev = {res.njev}")

    def get_state(self):
        return get_ansatz_state(self._params,
                        self._generators,
                        self._ref_state,
                        )

    def save_params(self):
        numpy.savetxt("optx.dat", self._params)

    def adjust_generators(self):
        # format conversion
        n = len(self._generators)
        if self._opspm:
            for i, ops in enumerate(self._generators):
                for j, cop in enumerate(ops):
                    _, op = cop
                    self._generators[i][j][1] = op.data.tocsr()

        # add more layers
        for i in range(1, self._nlayers):
            self._generators.extend(self._generators[:n])
        print(f'number of variational parameters: {len(self._generators)}')

    def set_ansatz(self):
        # commuting groups
        ncx = 0
        for labels in self._model._incar['op_ansatz']:
            ncx += get_ncnots(labels)
            self._generators.append(self._model.coeflabels2coefop(labels))
        print(f'n_cnots per layer = {ncx}')
        self.adjust_generators()

    def set_ref_state(self, ref_state, refmode):
        if ref_state is None:
            if refmode == 0:
            # default is the ground state of h1 in the correct valence sector.
                h = self._model.coeflabels2op(self._model._incar['h1ao'])
                npenalty = self._model.get_npenalty()
                if npenalty is not None:
                    h += npenalty
                w, v = h.groundstate()
                if npenalty is not None:
                    print(f"<n_penalty>: {abs(npenalty.matrix_element(v, v)):.2e}")
                self._ref_state = v.full().reshape(-1)
            elif refmode == 1:
                self._ref_state = numpy.ones((2**self._nq))
                self._ref_state /= numpy.linalg.norm(self._ref_state)
            else:
                self._ref_state = numpy.zeros((2**self._nq))
                ref = self._model._incar["ref_state"]
                # binary literal to int
                nnz = int(ref, 2)
                self._ref_state[nnz] = 1.
        else:
            if not isinstance(ref_state, numpy.ndarray) and self._opspm:
                ref_state = ref_state.full().reshape(-1)
            self._ref_state = ref_state


def fun_cost(params, generators_list, ref_state, h):
    state = get_ansatz_state(params, generators_list, ref_state)
    if isinstance(ref_state, numpy.ndarray):
        res = numpy.vdot(state, h.dot(state))
    else:
        res = h.matrix_element(state, state)
    res = res.real
    print(f"fun = {res:.8e}")
    return res.real


def fun_jac(params, generators_list, ref_state, h):
    # - d <var|h|var> / d theta
    vec = get_ansatz_state(params, generators_list, ref_state)
    opspm = isinstance(vec, numpy.ndarray)
    # <vec|h
    if opspm:
        h_vec = h.dot(vec)
    else:
        h_vec = h*vec

    jac = []
    state_ij = ref_state
    if opspm:
        for i, ops in enumerate(generators_list):
            zes = 0.
            for j, cop in enumerate(ops):
                coef, op = cop
                state_ij = numpy.cos(0.5*params[i]*coef)*state_ij - \
                        1j*numpy.sin(0.5*params[i]*coef)*op.dot(state_ij)
                state = coef*op.dot(state_ij)
                for copr in ops[j+1:]:
                    coef, opr = copr
                    state = numpy.cos(0.5*params[i]*coef)*state - \
                            1j*numpy.sin(0.5*params[i]*coef)*opr.dot(state)
                for theta, oprs in zip(params[i+1:], generators_list[i+1:]):
                    for copr in oprs:
                        coef, opr = copr
                        state = numpy.cos(0.5*theta*coef)*state - \
                                1j*numpy.sin(0.5*theta*coef)*opr.dot(state)
                zes += numpy.vdot(h_vec, state)
            jac.append(zes.imag)
    else:
        for i, ops in enumerate(generators_list):
            zes = 0.
            for j, cop in enumerate(ops):
                coef, op = cop
                opth = -0.5j*params[i]*coef*op
                state_ij = opth.expm()*state_ij
                state = coef*op*state_ij
                for copr in ops[j+1:]:
                    coef, opr = copr
                    opth = -0.5j*params[i]*coef*opr
                    state = opth.expm()*state
                for theta, oprs in zip(params[i+1:], generators_list[i+1:]):
                    for copr in oprs:
                        coef, opr = copr
                        opth = -0.5j*theta*coef*opr
                        state = opth.expm()*state
                zes += h_vec.overlap(state)
            jac.append(zes.imag)
    res = numpy.array(jac)
    print(f"jac max: {numpy.max(numpy.abs(res)):.2e}," +
            f" norm: {numpy.linalg.norm(res):.2e}")
    return res


def get_ansatz_state(params, generators_list, ref_state):
    state = ref_state
    opspm = isinstance(state, numpy.ndarray)
    for theta, ops in zip(params, generators_list):
        for cop in ops:
            coef, op = cop
            if opspm:
                state = numpy.cos(0.5*theta*coef)*state - \
                        1j*numpy.sin(0.5*theta*coef)*op.dot(state)
            else:
                opth = -0.5j*theta*coef*op
                state = opth.expm()*state
    return state


def get_ncnots(labels):
    ncx = 0
    for label in labels:
        label = label.split('*')[1]
        np = len(label) - label.count('I')
        if np > 1:
            ncx += 2*(np - 1)
    return ncx


def get_qubitwisecommutinggroups(labels):
    '''get qubit-wise subgroups with identity removed.
    '''
    groups = []
    touched = numpy.zeros(len(labels), dtype=bool)
    for _ in labels:
        group = None
        for i, label in enumerate(labels):
            if touched[i]:
                continue
            else:
                # remove identity
                op = label.split('*')[1]
                if op.count("I") == len(op):
                    touched[i] = True
                    continue

            if group is None:
                group = [label]
                touched[i] = True
            else:
                if qubitwisecommuting(label, group):
                    group.append(label)
                    touched[i] = True
        if group is None:
            assert(False not in touched), "not every op included."
            break
        else:
            groups.append(group)
    return groups


def qubitwisecommuting(label1, labels):
    op1 = label1.split('*')[1]
    for label in labels:
        op2 = label.split('*')[1]
        for s1, s2 in zip(op1, op2):
            if s1 == "I" or s2 == "I" or s1 == s2:
                continue
            else:
                return False
    return True
