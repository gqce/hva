#!/usr/bin/env python
import numpy, json
from model import model
from ansatz import ansatz
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-l", "--nlayers", type=int, default=1,
        help="nlayers of circuits. dflt: 1.")
parser.add_argument("--noopspm", action="store_true",
        help="use native qutip. not recommended. for debug only.")
parser.add_argument("-f", "--frac", type=float, default=7.,
        help="uniform initialization factor (pi/frac). dflt: 7")
parser.add_argument("-m", "--refmode", type=int, default=0,
        help="reference state mode. 0: gs of h1ao; 1: hf-like product state."+\
        " dflt: 0.")
parser.add_argument("-i", "--maxiter", type=int, default=None,
        help="max iteration. dflt: None.")
args = parser.parse_args()

opspm = not args.noopspm

nparam = len(json.load(open("incar", "r"))["op_ansatz"])
# identity initialization
params = numpy.ones(nparam*args.nlayers)*numpy.pi/args.frac

mdl = model()

ans = ansatz(mdl,
        opspm=opspm,
        nlayers=args.nlayers,
        params=params,
        refmode=args.refmode,
        maxiter=args.maxiter,
        )

print('reference state energy:', mdl.get_h_expval(ans._ref_state))
print('initial state energy:', mdl.get_h_expval(ans.get_state()))
ans.optimize_params()
w, v = mdl.get_loweste_states()
print("lowest energies: ", w)
print("cost:", ans._cost)
print("opt params:\n", numpy.round(ans._params, decimals=3))
ans.save_params()

if opspm:
    fidelity = abs(ans.get_state().dot(v[0].conj()))**2
else:
    fidelity = abs(ans.get_state().overlap(v[0]))**2
print("fidelity =", fidelity, flush=True)
