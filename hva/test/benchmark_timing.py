#timing benchmark
import numpy
from scipy.sparse.linalg import expm_multiply
from timeit import default_timer as timer
from hva.model import mfim
from hva.ansatz import spectra_data


nsite = 10
model = mfim(
        nsite=nsite,
        hx=-1.0,
        hz=-0.5,
        )
ref_state = numpy.sqrt(numpy.ones(2**nsite)/2**nsite)

start = timer()
ans1 = (0.3*model._h).expm().data.tocsr().dot(ref_state)
end = timer()
print(end - start)

h = spectra_data(model._h)
start = timer()
ans2 = h.expm(0.3).dot(ref_state)
end = timer()
print(end - start)

h = model._h.data.tocsr()
start = timer()
ans3 = expm_multiply(h, ref_state, start=0.3, stop=0.3*2, num=2)
end = timer()
print(end - start)

h = model._h.data.tocsc()*0.3
start = timer()
ans4 = expm_multiply(h, ref_state)
end = timer()
print(end - start)

print(ans3.shape)
print(numpy.max(numpy.abs(ans1 - ans2)),
        numpy.max(numpy.abs(ans1 - ans3[0])),
        numpy.max(numpy.abs(ans1 - ans4)),
        )
